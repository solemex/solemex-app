import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		login:false,
		datosUsuario:'',
		idusuariosweb: '',
	},

	mutations:{
		LOGEADO(state, value){
			state.login = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},
		ID_USUARIO(state, idusuariosweb){
			state.idusuariosweb = idusuariosweb
		}
	},

	actions:{
		//ejecuta la api y llama a la mutcion traeInfo para poder mandarle la información obtenida de la api
		validarUser({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
			  
			  Vue.http.post('api/v1/buscarusuario',{
					'email'	  : usuario.email,
					'password': usuario.password
				})

				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
	        	// console.log('res 1',respuestaJson)
	        	
					if(respuestaJson == null){
						// console.log('es null')
						resolve(false) 
						// commit('LOGEADO', false)
						return true
					}else{
						
						// console.log('no es  null')
						commit('LOGEADO', true)
	        	commit('DATOS_USUARIO',respuestaJson[0])
						resolve(respuestaJson) 
        	}
      	}, error => {
        	reject(error)
      	})
		})
	},



		saveInfoUsuario({dispatch, commit}, datosUsuario){
			// console.log(datosUsuario)
			Vue.http.put('auth/api/v1/usuariosweb/'+ datosUsuario.idusuariosweb, {				
				'nomuser'	: datosUsuario.Nomuser,
				'email'		: datosUsuario.Email,
				'telefono': datosUsuario.Telefono,
				'celular'	: datosUsuario.Celular,
				'empresa'	: "",
				'depto'		: datosUsuario.Departamento,
				'suc'     : ""
			})
			.then(function(response){
				// console.log(response.body)
				// var x = {email: datosUsuario.email, password: datosUsuario.password}
				// dispatch("validarUser",x)
			})


		}
},

	getters:{
		getLogeado(state){
		  return state.login
		},
		getdatosUsuario(state){
			return state.datosUsuario
		},

		getidUsuariosWeb(state){
			return state.idusuariosweb
		}
	}
}