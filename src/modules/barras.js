import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		menu: true,
	},

	mutations:{
		//actualiza los articulos con la informacion que le manda el action
		MENU(state, data){
      state.menu = data
		},
	},

	actions:{		
		//ejecuta la api y llama a la mutcion traeInfo para poder mandarle la información obtenida de la api
		showBarra({commit}, data){
      commit('MENU', data)
		},

	},

	getters:{
		//trae la información de usuario despues de haber sido mutada
		getMenu(state){
			return state.menu
		},
	}
}