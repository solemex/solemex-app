import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		registro:''
	},

	mutations:{

		ACTUALIZREGISTRO(state, registro){
			state.registro = registro
		}

	},

	actions:{

		// BUSCAR USUARIO
		buscarUsuarioWeb({commit, dispatch}, usuario){
			
			return new Promise((resolve, reject) => {
				
				const FormData = {'Email': usuario.email}
			
				Vue.http.post('api/v1/getidxmail',  FormData) 
					.then(respuesta=>{return respuesta.json()
				})
				
				.then(respuestaJson=>{
				// VALIDO LA RESPUESTA
			if(respuestaJson.idusuariosweb == ""){
				// SI NO ENCUENTRA AL USUARIO MANDA A REGISTRARLO
				console.log('El usuario no exite.')
				resolve(true)

			}else{
				// SI ENCUENTRO AL USUARIO REGRESO FALSO
				resolve(false)
			}

			  }).catch(function(error){console.log('error',error)})
			})
		},

		registrarUsuario({commit}, usuario){
			
			return new Promise((resolve, reject) => {
				
				const FormData = {
					Nomuser:      usuario.nomuser,
					Email:        usuario.email,
					Nivel:        "",
					Telefono:     "",
					Celular: 	    "",
					Empresa:      "",
					Depto:        "",
					Suc:          "",
					Password:     usuario.password,
					Numcli:       "0",
					Estatus: 	  "1",
					Idnivel :     3
				}

				console.log(FormData)
				Vue.http.post('api/v1/usuariosweb',  FormData) 
				.then(respuesta=>{ return respuesta.json() })
				
				.then(respuestaJson=>{
					if(respuestaJson === "Insertado correctamente"){
						resolve(true)

					}else{
						console.log('No se inserto.')
						resolve(false)
					}

			  }).catch(function(error){console.log('error',error)})
			})
		},
	},

	getters:{
		
		traerDatosUsuarios(state){
			return state.registro
		}

	}
}