import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		mensajeros:[],
		supervision: [],
	},

	mutations:{

		LISTARMENSAJEROS(state,mensajeros){
			state.mensajeros	=	mensajeros
		},
		LISTARSUPERVISORES(state,supervision){
			state.supervision	=	supervision
		},
	},

	actions:{

		// Actualizar mensajeros
		ActualizaMensajeros({commit}, payload){
			// console.log('ActualizaMensajeros', payload)
			commit('LISTARMENSAJEROS', payload)
		},
		// Actualizar mensajeros
		ActualizaSupervision({commit}, payload){
			commit('LISTARSUPERVISORES', payload)
		}

	},

	getters:{
		
		cargarMensajeros(state){
			return state.mensajeros
		},

		cargarSupervision(state){
			return state.supervision
		}

	}
}