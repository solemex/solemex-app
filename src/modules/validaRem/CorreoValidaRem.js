import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		cuerpo: ''
	},

	mutations:{

		CUERPOCORREO(state,cuerpo){
			state.cuerpo	=	cuerpo
		},
	},

	actions:{

		// TRAER TODAS LAS REMESAS
		cargar_info_remesas({commit}){
			return new Promise((resolve,reject) =>{
				Vue.http.get('api/v1/remesas').then(response =>{
					return response.json()
				}).then(responsejson =>{
				
					commit('LISTARREMESAS', responsejson)
					resolve(true)
	
				}).catch(function(error){
					console.log(error)
				})
			})

		},

		// AGREGAR UN NUEVA REMESA
		addRemesa({dispatch}, remesas){
			Vue.http.post('api/v1/remesas',{
				// 'idclasif'	: remesas.idclasif

			}) 
			.then(function(response){
				dispatch("cargar_info_remesas")
			})
		},

		// BUSCAR UNA remesas 
		busIdRemesa({commit}, id){
			console.log(id)
			Vue.http.get('api/v1/guiasrem/' + id)
				.then(function(response){
					console.log('respuesta',response.body)
					commit('SAVE_INFO_A_EDIT', response.body)
					})
					.catch(function(error){
					console.log(error)
				})
		},

		// MODIFIAR UN REMESA
		modiRemesa({commit}, payload){
			Vue.http.put('api/v1/remesas/'  + payload.idRemesa,{
				// 'idclasif'	: payload.idclasif,

			})

			.then(function(response){
				console.log(response.body)
			}).catch(function(error){
				console.log(error)
			})
		}

	},

	getters:{
		
		cargarRemesas(state){
			return state.remesas
		},

		cargarRemesaAEdit(state){
			return state.remesaAEditar
		}
	}
}