import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		usuariosweb:[],
	},

	mutations:{

		LISTARUSUARIOSWEB(state,usuariosweb){
			state.usuariosweb	=	usuariosweb
		},
	},

	actions:{

		// Actualizar usuariosweb
		ActualizaUsuariosWeb({commit}, payload){
			// console.log('Actualizo información', payload)
			commit('LISTARUSUARIOSWEB', payload)
		}

	},

	getters:{
		
		cargarUsuariosWeb(state){
			return state.usuariosweb
		}

	}
}