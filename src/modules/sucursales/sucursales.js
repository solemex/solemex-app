import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		sucursales:[],
	},

	mutations:{

		LISTARSUCURSALES(state,sucursales){
			state.sucursales	=	sucursales
		},
	},

	actions:{

		// Actualizar sucursales
		ActualizaSucursales({commit}, payload){
			// console.log('Actualizo información', payload)
			commit('LISTARSUCURSALES', payload)
		}

	},

	getters:{
		
		cargarSucursales(state){
			return state.sucursales
		}

	}
}