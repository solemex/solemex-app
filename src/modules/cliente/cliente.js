import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import axios from 'axios'


export default{
	namespaced: true,
	state:{
		clientes:[],
		clienteAEditar:[]
	},

	mutations:{

		SAVE_INFO_A_EDIT(state, clienteAEditar){
			state.clienteAEditar	= clienteAEditar
		},

	},

	actions:{
	
		// AGREGAR UN NUEVO CLIENTE
		addCliente({commit}, payload){
			const cliente = {
				'numcli'	: payload.Numcli,
				'nomcli'	: payload.Nomcli,
				'numvend'	: payload.numvend,
				'numzona'	: payload.numzona,  
				'Numclasif'	: payload.Numclasif,
				'Numdiacon' : payload.Numdiacon,
				'empleados'	: payload.empleados		
			}
			
			Vue.http.post('api/v1/clientes', cliente)
				.then(function(response){
					console.log("datos", response.body)
				})
				.catch(function(error){
				   console.log(error)
				})
		},


		// BUSCAR UN CLIENTE 
		busIdCliente({ commit }, id){
			console.log(id)
			Vue.http.get('api/v1/clientes/' + id)
				.then(function(response){	
				console.log('respuesta', response.body)			
					commit('SAVE_INFO_A_EDIT', response.body)
				}).catch(function(error){
					console.log(error)
				})
		},

		// MODIFIAR UN CLIENTE 
		modiCliente({dispatch}, clientes){
			Vue.http.put('api/v1/clientes/' + clientes.ID,{
				'Nomcli'	: clientes.Nomcli,
				'Calle'		: clientes.Calle,
				'Numext'	: clientes.Numext,
				'Colonia'	: clientes.Colonia,  
				'Ciudad'	: clientes.Ciudad,
				'CP' 		: clientes.CP,
				'Estado'    : clientes.Estado,
				'Telefono' 	: clientes.Telefono,
				'Email' 	: clientes.Email
			})
			.then(function(response){
				// dispatch("cargar_info_clientes")
			}).catch(function(error){
				console.log(error)
			})
		},

	},

	getters:{
		
		cargarClienteAEdit(state){
			console.log('Cargando Cliente')
			return state.clienteAEditar
		}
	}

}