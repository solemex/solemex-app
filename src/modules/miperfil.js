import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		usuario:'',
	},

	mutations:{
		//actualiza los articulos con la informacion que le manda el action
		LISTUSUARIOS(state, usuario){
      state.DatosUsuario = usuario
		},
	},

	actions:{		
		
		//ejecuta la api y llama a la mutcion traeInfo para poder mandarle la información obtenida de la api
		getdatosUsuario({commit}, usuario){
			Vue.http.get('api/v1/usuarios').then(function(response){
      		this.someData = response.body
    			commit('LISTUSUARIOS', response.body)

      	}).catch(function(error){
          console.log(error)
      	})
		},

		saveInfoUsuario({dispatch}, usuario){
			console.log(usuario)
			Vue.http.put('api/v1/usuarios/'+ usuario.idusuariosweb, {
				'nomuser'			: usuario.Nomuser,
				'email'   		: usuario.Email,
				'telefono'		: usuario.Telefono,
				'celular'			: usuario.Celular,
				'empresa'			: usuario.Empresa,
				'departamento': usuario.Departamento,
				'sucursal'		: usuario.Sucursal
			})
			.then(function(response){
				// var x = {email: datosUsuario.email, password: datosUsuario.password}
				// dispatch("validarUser",x)
			})
		}
	},

	getters:{
		//trae la información de usuario despues de haber sido mutada
		traeUsuario(state){
			return state.DatosUsuario
		},
	}
}