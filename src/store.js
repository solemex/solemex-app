import Vue 	from 'vue'
import Vuex from 'vuex'

//import VueClipboard from 'vue-clipboard2'
import axios from 'axios'
import VueAxios from 'vue-axios'

//import miperfil from '@/modules/miperfil'
import rutas    from './router'
import router   from '@/router'
import decode   from 'jwt-decode'       //seguridad

import login    from '@/modules/login'
import registro from '@/modules/registro'	

// import rutas    from './router'
import cliente			from '@/modules/cliente/cliente'
import remesas			from '@/modules/remesas/remesas'
import barras			  from '@/modules/barras'

import mensajeros	from '@/modules/mensajeros/mensajeros'
import usuariosweb  from '@/modules/usuariosweb/usuariosweb'
import sucursales 	from '@/modules/sucursales/sucursales'


Vue.use(Vuex)

export default new Vuex.Store({
	state: {
	    token: '',   
	    usuario: '',
	    // token: null,   
	    // usuario: null,
	    nivel: null,
	    acuses:'',
	    drawer: true,
	    menu: true,
	  },

	mutations: {
	    setToken(state, token){
	    	// console.log('recibo token', token)
	      state.token= token
	    },
	    setUsuario (state, usuario){
	    	// console.log('serUsuario', usuario)
	      state.usuario = usuario
	    },
	    setNivel (state, nivel){
	    	// console.log('setNivel', nivel)
	      state.nivel=  nivel
	    }

    },

	actions: {
		
		// Se manda llamar desde Login.vue
	    guardarToken({commit},formData){

	      var Token = formData.token  // Asigno el token a una variable
	      var Nivel = formData.nivel  // Asigno el nivel a una variable

	      commit("setToken", Token) 			// Guarda el Token que se recibe
	      commit("setUsuario", decode(Token))   // Decodifica el token para sacar usuario
		  commit("setNivel", Nivel)
	      localStorage.setItem("apiKey_admin", Token)
	    },

	    getUserId(){
	    	
	    },
	    
	    autoLogin({commit}){
	     //  let token = localStorage.getItem("token")
	     // if (token){
	     //    commit("setToken", token)
	     //    commit("setUsuario", decode(token))
	     //  }
	     //  router.push({name: 'login'})
	    },

	    salir({commit}){
	      commit("setToken", '')
	      commit("setUsuario", '')
	      localStorage.removeItem("token")
	      router.push({name: 'login'})
	    }
	  },

  	getters:{
		traeNomuser(state){
			// console.log('traeNomuser')
			return state.usuario
		},
		traeNivel(state){
			// console.log('traeNivel')

			return state.nivel
		},

	},


	modules:{
		login,
		remesas,
		registro,
		cliente,
		barras,
		mensajeros,
		usuariosweb,
		sucursales
	}
})

