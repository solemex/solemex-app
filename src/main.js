import Vue from 'vue'
//libreria resource que nos ayudara a hacer las consutas
import VueResource from 'vue-resource'


//Plugins
import './plugins/vuetify'
//import './plugins/cliboard'

// Inicio de APP
import App from './App.vue'
import router from './router'
import store from './store'

/// HTML TO PAPER TEST
import VueHtmlToPaper from 'vue-html-to-paper';
 
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}

Vue.use(VueHtmlToPaper, options);
Vue.config.productionTip = false
Vue.use(VueResource)


//esta es la configración para una url constante la cual se ejecuta antes del vue
// Vue.http.options.root = 'http://localhost:8085/sol'
Vue.http.options.root = 'https://solemex.ws/sol'

Vue.http.interceptors.push((request, next) => {
 // request.headers.set('Authorization', 'Bearer '+JSON.parse(localStorage.apiKey_admin))
  request.headers.set('Accept', 'application/json')
  next()
});


new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')


