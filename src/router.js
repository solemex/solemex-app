import Vue          from 'vue'
import Router       from 'vue-router'

//Ruter y store
import store from './store'
import rutas from './router'

// VISTAS PRINCIPALES
import Login    from './views/Login.vue'
import MyPerfil from './views/MyPerfil.vue'
import Home     from './views/Home.vue'

// <<<<<<<<<<<<<ADMINISTRACION<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// REMESAS
import CatRemesas  from './views/remesas/CatRemesas.vue'
import NewRemesa   from '@/views/validarem/NewRemesa.vue'
import tarjetas    from './views/remesas/tarjetas.vue'
import RastreoTarj from './views/remesas/RastreoTarj.vue'

// TARJETAS
import CatTarjetas from './views/tarjetas/CatTarjetas.vue'
import NewTarjeta  from  './views/tarjetas/NewTarjeta.vue'
import EditTarjeta from './views/tarjetas/EditTarjeta.vue'

// ASIGNAR PLAZAS
import AsignarPlaza from '@/views/administra/asignarplaza/AsignarPlaza.vue'

// RASTREO DE TARJETAS
import RastreoTarjeta from './views/rastreo/RastrearTarjeta.vue'

// VALIDAR REMESAS
import Validar from '@/views/validarem/Validar.vue'

// ASIGNAR MENSAJERO
import Asignamen from '@/views/asignacion/asignamen.vue'

// mensajerias
import NewMensaje from '@/views/mensajerias/NewMensaje.vue'

/// SOLO LOS CATALOGOS VAN EN LAZY LOAD
// CONSULTAS PRINCIPALES VAN DIRECTAS: PREGUNTAR: PREGUNTAR
// Volver a hacer


Vue.use(Router)

var router = new Router ({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'login', component:Login,
      meta: { libre: true }},
  
    { path: '/registro', name: 'registro', component:()=> import('./views/Registro.vue') ,
      meta: { libre: true }} ,

    { path: '/miperfil', name: 'miperfil', component: MyPerfil,
      meta: { banco: true, solemex: true, sait: true , mensajeria: true }},

    { path: '/home', name: 'home', component:Home , 
      meta: { banco: true, solemex: true, sait: true , mensajeria: true }},
      

    // CLIENTES
    { path: '/catcliente',      name: 'catcliente'  , component:() => import('./views/clientes/CatCliente'),
      meta: { solemex: true, sait: true } },
    { path: '/newcliente',      name: 'newcliente'  , component:() => import('./views/clientes/NewCliente'),
      meta: { solemex: true, sait: true }  },
    { path: '/editcliente',     name: 'editcliente' , component:() => import('@/views/clientes/EditCliente'),
      meta: { solemex: true, sait: true } },

    
    // VALIDA REM
    { path: '/validar', name: 'validar'    , component: Validar,
      meta: { solemex: true, sait: true, mensajeria: true } },

    // { path: '/imprimiracuses' , name: 'imprimiracuses', component:() => import('@/views/validarem/ImprimirAcuses.vue'),
    //   meta: { solemex: true, sait: true } },

    { path: '/impacuse' , name: 'impacuse', component:() => import('@/views/Acuses/ImpAcuse.vue'),
      meta: { solemex: true, sait: true } },      

    { path: '/correovalidarem' , name: 'correovalidarem', component:()=> import('@/views/validarem/correo/CorreoValidaRem.vue'),
      meta: { solemex: true, sait: true } },

      // VALIDACION DIRECTA
    { path: '/devoluciondirecta', name: 'devoluciondirecta'    , component:() => import('@/views/DevolucionDirecta/DevolucionDirecta.vue'),
      meta: { solemex: true, sait: true, mensajeria: true } },
// >>>>>>>>>>>>>CATALOGOS>>>>>>>><<<

    // REMESAS
    { path: '/catremesas'   , name: 'catremesas'   , component: CatRemesas,
      meta: { solemex: true, sait: true, banco: true } },

    { path: '/newremesa'    ,  name: 'newremesa'   , component:NewRemesa,
      meta: { solemex: true, sait: true, banco: true } },

    { path: '/tarjetas'     , name: 'tarjetas'     , component: tarjetas,
      meta: { solemex: true, sait: true, banco: true} },

    { path: '/rastreotarj'  , name: 'rastreotarj'  , component: RastreoTarj,
      meta: { solemex: true, sait: true, banco: true} },

    { path: '/impremesa',  name: 'impremesa'  , component:() => import('./views/remesas/ImpRemesa.vue'), 
      meta: { solemex: true, sait: true } },

    { path: '/expexcel',   name: 'expexcel'  , component:() => import('./views/remesas/ExportarExcel.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/newremesaNOusar',  name: 'newremesaNOusar'  , component:() => import('./views/remesas/NewRemesaNOusar.vue'),
      meta: { solemex: true, sait: true } }, 

    { path: '/avanceremesa',      name: 'avanceremesa'  , component:() => import('./views/remesas/AvanceRemesa'),
      meta: { solemex: true, sait: true , banco: true} },

    { path: '/detalleremesa',      name: 'detalleremesa'  , component:() => import('./views/remesas/DetalleRemesa'),
      meta: { solemex: true, sait: true, banco: true } },
  // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // TARJETAS
    { path: '/cattarjetas',  name: 'cattarjetas', component: CatTarjetas,
      meta: { solemex: true, sait: true, banco: true } },

    { path: '/newtarjeta',  name: 'newtarjeta'  , component: NewTarjeta,
      meta: { solemex: true, sait: true } },

    { path: '/edittarjeta', name: 'edittarjeta' , component: EditTarjeta,
      meta: { solemex: true, sait: true } },

    { path: '/imptarjeta',  name: 'imptarjeta'  , component:()=> import('./views/tarjetas/ImpTarjeta.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/acuses',      name: 'acuses'      , component: () => import( '@/views/tarjetas/Acuses.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/tarjsinsuc',      name: 'tarjsinsuc'      , component: () => import( '@/views/tarjetas/TarjSinSuc.vue'),
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // MENSAJEROS
    { path: '/catmensajeros', name: 'catmensajeros' , component: () => import( '@/views/mensajeros/CatMensajeros.vue'),
      meta: { solemex: true, sait: true, banco: true} },
    { path: '/newmensajero',  name: 'newmensajero'  , component: () => import( '@/views/mensajeros/NewMensajero.vue'),
      meta: { solemex: true, sait: true } }, 
    { path: '/editmensajero', name: 'editmensajero' , component: () => import( '@/views/mensajeros/EditMensajero.vue'), 
      meta: { solemex: true, sait: true } },
    { path: '/impmensajero',  name: 'impmensajero'  , component: () => import( '@/views/mensajeros/ImpMensajero.vue'), 
      meta: { solemex: true, sait: true } },
    { path: '/supervisionmjero',  name: 'supervisionmjero'  , component: () => import( '@/views/mensajeros/SupervisionMjero.vue'), 
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
   
    // PENDIENTES POR MENSAJERO
    { path: '/pendxmensajero',  name: 'pendxmensajero'  , component:()=> import('./views/pendxMensajero/PendxMensajero.vue'),
      meta: { solemex: true, sait: true, mensajeria: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    
    //ASIGNACION
    { path: '/asignamen',     name: 'asignamen'     , component: Asignamen,
      meta: { solemex: true, sait: true, mensajeria:true } },

    { path: '/rutapend',      name: 'rutapend'      , component: ()=> import('@/views/rutapend/rutapend.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/conruta',       name: 'conruta'       , component:()=> import('@/views/rutapend/conruta.vue'),
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // ,MENSAJERIA
    { path: '/catmensajerias', name: 'catmensajerias' , component: () => import( '@/views/mensajerias/CatMensajerias.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/newmensaje',     name: 'newmensaje'     , component: NewMensaje ,
      meta: { solemex: true, sait: true } },

    { path: '/editmensaje',   name: 'editmensaje'     , component: () => import( '@/views/mensajerias/EditMensaje.vue'), 
      meta: { solemex: true, sait: true } },

    { path: '/impmensaje',    name: 'impmensaje'      , component: () => import( '@/views/mensajerias/ImpMensaje.vue'),  
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // PLAZAS
    { path: '/catplazas',   name: 'catplazas'   , component: () => import( '@/views/plazas/CatPlazas.vue'), 
      meta: { solemex: true, sait: true } },

    { path: '/newplaza',    name: 'newplaza'    , component: () => import( '@/views/plazas/NewPlaza.vue'),  
      meta: { solemex: true, sait: true } }, 

    { path: '/editplaza',   name: 'editplaza'   , component: () => import( '@/views/plazas/EditPlaza.vue'), 
      meta: { solemex: true, sait: true } },

    { path: '/impplaza',    name: 'impplaza'    , component: () => import( '@/views/plazas/ImpPlaza.vue'), 
      meta: { solemex: true, sait: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
   
    // SUCURSALES
    { path: '/catsucursales', name: 'catsucursales', component:()=> import('./views/sucursales/CatSucursales.vue'),
      meta: { solemex: true, sait: true, banco: true} },

    { path: '/newsucursal',   name: 'newsucursal'  , component:()=> import('./views/sucursales/NewSucursal.vue'),
      meta: { solemex: true, sait: true, banco: true } },

    { path: '/editsucursal',   name: 'editsucursal'  , component:()=> import('./views/sucursales/EditSucursal.vue'),
      meta: { solemex: true, sait: true, banco: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // USUARIOS
    { path: '/catusuarios', name: 'catusuarios' , component:()=> import('./views/usuarios/CatUsuarios.vue'),
      meta: { solemex: true, sait: true, banco: true} },
    { path: '/newusuario',  name: 'newusuario' , component:()=> import('./views/usuarios/NewUsuario.vue'),
      meta: { solemex: true, sait: true, banco: true } },
    { path: '/editusuario',  name: 'editusuario' , component:()=> import('./views/usuarios/EditUsuario.vue'),
      meta: { solemex: true, sait: true, banco: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  
    
    // NIVELES
    { path: '/catniveles', name: 'catniveles' , component:()=> import('./views/niveles/CatNiveles.vue'),
      meta: { solemex: true, sait: true } },
    { path: '/newnivel'  , name: 'newnivel'    , component:()=> import('./views/niveles/NewNivel.vue'),
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
   
    // SERVICIOS
    { path: '/catservicios' , name: 'catservicios' , component:()=> import('./views/servicios/CatServicios.vue'),
      meta: { solemex: true, sait: true } },
    { path: '/newservicio'  , name: 'newservicio'  , component:()=> import('./views/servicios/NewServicio.vue'),
      meta: { solemex: true, sait: true } },

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    
    // AVISOS
    { path: '/avisos'  , name: 'avisos'  , component:()=> import('@/views/avisos/avisos.vue'),
      meta: { solemex: true, sait: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
   
    //RASTREO DE TARJETAS
    { path: '/rastreartarjeta'  , name: 'rastreartarjeta'  , component: RastreoTarjeta,
      meta: { solemex: true, sait: true, banco: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // ASIGNAR PLAZA
    { path: '/asignarplaza'  , name: 'asignarplaza'  , component: AsignarPlaza,
      meta: { solemex: true, sait: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //Correos e imagenes
    { path: '/correos'      , name: 'correos'      , component:()=> import('@/views/funcionalidades/Correos.vue'),
      meta: { solemex: true, sait: true } },

    { path: '/subirimagen'  , name: 'subirimagen'  , component:()=> import('@/views/funcionalidades/SubirImagen.vue'),
      meta: { solemex: true, sait: true } },
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // PRUEBAS
    { path: '/pruebas'  , name: 'pruebas'  , component:()=> import('@/views/pruebas.vue'),
      meta: { sait: true, solemex: true, } },

// >>>>>>>>>>>>>>>>>>>>>>>RUTAS DINAMICAS EXTERNAS
    // ActivarUsuario
    { path: '/activarusuario/:id'  , name: 'activarusuario'  , component:()=> import('@/views/externos/ActivarUsuario.vue') ,
      meta: { libre: true }},

    { path: '/linkrastreo/:numguia'  , name: 'linkrastreo'  , component:()=> import('@/views/externos/linkrastreo.vue') ,
      meta: { libre: true }},

    { path: '/linkintentos/:numguia'  , name: 'linkintentos'  , component:()=> import('@/views/externos/linkintentos.vue') ,
      meta: { libre: true }},

    { path: '/linkasigna/:numguia'  , name: 'linkasigna'  , component:()=> import('@/views/externos/linkasigna.vue') ,
      meta: { libre: true }},

    { path: '/linkentregasuc/:numguia'  , name: 'linkentregasuc'  , component:()=> import('@/views/externos/linkentregasuc.vue') ,
      meta: { libre: true }},

    { path: '/misenvios/:numguia'  , name: 'misenvios'  , component:()=> import('@/views/externos/MisEnvios.vue') ,
      meta: { libre: true }},
    
    { path: '/rastreo'  , name: 'rastreo'  , component:()=> import('@/views/externos/Rastreo.vue') ,
      meta: { libre: true }}

  ]
})


router.beforeEach( (to, from, next) => {

  // infica a que ruta voy a acceder
  // matched.some = compara si el meta es libre
  if(to.matched.some(record => record.meta.libre)){
    next()

  }else if(store.state.usuario && store.state.nivel == "SAIT"){

    if(to.matched.some(record => record.meta.sait)){

      next()
    }

  }else if(store.state.usuario && store.state.nivel == "SOLEMEX"){
    if(to.matched.some(record => record.meta.solemex)){
      next()
    }

  }else if(store.state.usuario && store.state.nivel == "BANCO"){
    if(to.matched.some(record => record.meta.banco)){
      next()
    }

  }else if(store.state.usuario && store.state.nivel == "MENSAJERIA"){
    if(to.matched.some(record => record.meta.mensajeria)){
      next()
    }

  }
  else{
    next({
      name: 'login'

    })
  }
})

export default router 


//  let token = localStorage.getItem("token")
       // if (token){
       //    commit("setToken", token)
       //    commit("setUsuario", decode(token))
       //  }
       //  router.push({name: 'login'})

/*
const router =  new Router({
  rutas
})

//to de donde viene
// from a donde quiere ir
// next continuar

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isLogged = store.state.login.logeado
  if ( ! requiresAuth && isLogged && to.path === '/login'){
    next('/prueba')
  }
  if(requiresAuth && ! isLogged){
    next('/login')
  }else{
    next()
  }
})

export default router*/